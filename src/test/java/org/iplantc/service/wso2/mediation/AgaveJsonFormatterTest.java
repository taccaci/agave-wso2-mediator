package org.iplantc.service.wso2.mediation;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMXMLBuilderFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.Test;

public class AgaveJsonFormatterTest 
{
	@Test
	public void formatJson() throws JSONException, IOException {
	    String xml = "<am:fault xmlns:am=\"http://wso2.org/apimanager\"><am:code>404</am:code><am:type>Status report</am:type><am:message>Not Found</am:message><am:description>The requested resource (/agave/2.1) is not available.</am:description></am:fault>";
	    InputStream in = new ByteArrayInputStream(xml.getBytes());
	    OMElement root = OMXMLBuilderFactory.createOMBuilder(in).getDocumentElement();
	
	    Iterator<OMElement> children = (Iterator<OMElement>)root.getChildElements();
	    while (children.hasNext()) 
	    {
	    	OMElement child = children.next();
	    	System.out.println(child.getLocalName());
	    	if (child.getLocalName().equals("description")) {
	    		System.out.write(AgaveJsonFormatter.formatJson(
	    				new JSONObject().put(
	    						"fault", new JSONObject().put(
	    								"description", child.getText())).toString()).getBytes());
	    	}
	    }
	}
}
