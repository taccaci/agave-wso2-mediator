package org.iplantc.service.wso2.mediation;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Iterator;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.axiom.om.OMDataSource;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMOutputFormat;
import org.apache.axiom.om.impl.llom.OMElementImpl;
import org.apache.axiom.om.impl.llom.OMSourcedElementImpl;
import org.apache.axiom.soap.SOAPFault;
import org.apache.axis2.AxisFault;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.json.AbstractJSONMessageFormatter;
import org.apache.axis2.json.JSONDataSource;
import org.apache.log4j.Logger;
import org.codehaus.jettison.mapped.MappedNamespaceConvention;
import org.codehaus.jettison.mapped.MappedXMLStreamWriter;
import org.json.JSONObject;

public class AgaveJsonFormatter extends AbstractJSONMessageFormatter {
	private static final Logger log = Logger.getLogger(AgaveJsonFormatter.class);
	
	
	//returns the "Mapped" JSON writer
    @Override
    protected XMLStreamWriter getJSONWriter(Writer writer) {
        MappedNamespaceConvention mnc = new MappedNamespaceConvention();
        return new MappedXMLStreamWriter(mnc, writer);
    }

    /**
     * If the data source is a "Mapped" formatted data source, gives the JSON string by directly
     * taking from the data source.
     *
     * @param dataSource data source to be checked
     * @return the JSON string to write
     */
    @Override
    protected String getStringToWrite(OMDataSource dataSource) 
    {
        if (dataSource instanceof JSONDataSource) 
        {
        	return formatJson(((JSONDataSource)dataSource).getCompleteJOSNString());
			
        } else {
            return null;
        }
    }
    
    protected static String formatJson(String sJson) 
    {
    	JSONObject json = new JSONObject(sJson);
		
		if (json.has("fault") && json.getJSONObject("fault").has("description")) 
		{
			JSONObject agaveJson = new JSONObject();
			agaveJson.put("status", "error");
			agaveJson.put("message", json.getJSONObject("fault").getString("description"));
			agaveJson.put("version", Settings.SERVICE_VERSION);
			agaveJson.put("result", (String)null);
			return agaveJson.toString();
		}
		else
		{
			log.debug("Unexpected json response: " + json.toString());
			return json.toString();
		}
    }
	
	@Override
	public byte[] getBytes(MessageContext msgCtxt, OMOutputFormat format)
	throws AxisFault 
	{
		OMElement element = msgCtxt.getEnvelope().getBody().getFirstElement();
        //if the element is an OMSourcedElementImpl and it contains a JSONDataSource with
        //correct convention, directly get the JSON string.

        if (element instanceof OMSourcedElementImpl &&
                getStringToWrite(((OMSourcedElementImpl)element).getDataSource()) != null) {
            String jsonToWrite = getStringToWrite(((OMSourcedElementImpl)element).getDataSource());
            return formatJson(jsonToWrite).getBytes();
            //otherwise serialize the OM by expanding the tree
        } else {
            try {
                ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();
                XMLStreamWriter jsonWriter = getJSONWriter(bytesOut, format);
                element.serializeAndConsume(jsonWriter);
                jsonWriter.writeEndDocument();
                
                return formatJson(new String(bytesOut.toByteArray())).getBytes();

            } catch (XMLStreamException e) {
                throw AxisFault.makeFault(e);
            } catch (FactoryConfigurationError e) {
                throw AxisFault.makeFault(e);
            } catch (IllegalStateException e) {
                throw new AxisFault(
                        "Mapped formatted JSON with namespaces are not supported in Axis2. " +
                                "Make sure that your request doesn't include namespaces or " +
                                "use the Badgerfish convention");
            }
        }
	}
	
	private XMLStreamWriter getJSONWriter(OutputStream outStream, OMOutputFormat format)
            throws AxisFault {
        try {
            return getJSONWriter(new OutputStreamWriter(outStream, format.getCharSetEncoding()));
        } catch (UnsupportedEncodingException ex) {
            throw AxisFault.makeFault(ex);
        }
    }

	@SuppressWarnings("unchecked")
	@Override
	public void writeTo(MessageContext msgCtxt, OMOutputFormat format, OutputStream out, boolean preserve) 
	throws AxisFault 
	{	
		OMElement element = msgCtxt.getEnvelope().getBody().getFirstElement();
        try {
            //Mapped format cannot handle element with namespaces.. So cannot handle Faults
            if (element instanceof SOAPFault) {
                SOAPFault fault = (SOAPFault)element;
                log.debug(element.toString());
                OMElement element2 = new OMElementImpl("Fault", null, element.getOMFactory());
                element2.setText(fault.toString());
                element = element2;
            }
            if (element instanceof OMSourcedElementImpl &&
                    getStringToWrite(((OMSourcedElementImpl)element).getDataSource()) != null) {
                String jsonToWrite =
                        getStringToWrite(((OMSourcedElementImpl)element).getDataSource());

                out.write(formatJson(new String(jsonToWrite.getBytes())).getBytes());
                out.flush();
            } else {
                Iterator<OMElement> children = (Iterator<OMElement>)element.getChildElements();
                while (children.hasNext()) {
                	OMElement child = children.next();
                	if (child.getLocalName().equals("description")) {
                		out.write(formatJson(
                				new JSONObject().put(
                						"fault", new JSONObject().put(
                								"description", child.getText())).toString()).getBytes());
                	}
                }
                out.flush();                
            }
        } catch (IOException e) {
            throw AxisFault.makeFault(e);
        } catch (IllegalStateException e) {
            throw new AxisFault(
                    "Mapped formatted JSON with namespaces are not supported in Axis2. " +
                            "Make sure that your request doesn't include namespaces or " +
                            "use the Badgerfish convention");
        }
	}

}