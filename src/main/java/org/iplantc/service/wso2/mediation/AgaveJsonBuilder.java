/**
 * 
 */
package org.iplantc.service.wso2.mediation;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.soap.SOAPEnvelope;
import org.apache.axiom.soap.SOAPFactory;
import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.builder.Builder;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.transport.http.util.URIEncoderDecoder;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;

/**
 * @author dooley
 *
 */
public class AgaveJsonBuilder implements Builder 
{
    public OMElement processDocument(InputStream inputStream, String s, MessageContext messageContext) 
    throws AxisFault 
    {
        SOAPFactory factory = OMAbstractFactory.getSOAP12Factory();
        SOAPEnvelope envelope = factory.getDefaultEnvelope();

        if (inputStream != null) 
        {
            messageContext.setProperty("JSON_STREAM", inputStream);
        } 
        else 
        {
            EndpointReference endpointReference = messageContext.getTo();
            if (endpointReference == null) {
                throw new AxisFault("Cannot create DocumentElement without destination EPR");
            }
            String requestURL;
            try {
                requestURL = URIEncoderDecoder.decode(endpointReference.getAddress());
            } catch (UnsupportedEncodingException e) {
                throw AxisFault.makeFault(e);
            }

            String jsonString;
            int index;
            //As the message is received through GET, check for "=" sign and consider the second
            //half as the incoming JSON message
            if ((index = requestURL.indexOf("=")) > 0) {
                jsonString = requestURL.substring(index + 1);
                messageContext.setProperty("JSON_STRING", jsonString);
            } else {
                throw new AxisFault("No JSON message received through HTTP GET or POST");
            }
        }

        return envelope;
    }
}
