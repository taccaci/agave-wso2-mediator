package org.iplantc.service.wso2.mediation;

import java.util.Properties;

public class Settings 
{
	private static final String PROPERTY_FILE = "service.properties";
	
	/* Debug settings */
	public static String		API_VERSION;
	public static String		SERVICE_VERSION;
		
	static
	{
		Properties props = new Properties();
		try
		{
			props.load(Settings.class.getClassLoader().getResourceAsStream(PROPERTY_FILE));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		API_VERSION = (String)props.getProperty("iplant.api.version");
		
		SERVICE_VERSION = (String)props.getProperty("iplant.service.version");
	}
}
