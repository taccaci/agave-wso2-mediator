# Agave WSO2 Response Mediator

## Introduction
This project contains the classes necessary to serialized json responses from the wso2 API Manager into the standard Agave JSON response format. Without installing this jar and configuring its use, multiple repsonses will come back from the API Manager in XML and inconsistent JSON format.

## Building
	Maven 3
	Java 6
	WSO2 API Manager 1.5

I order to build this project, you will need to have Maven 3 installed as well as Java 6. This project should build as part of the overall Agave API build and deployment step. To build the project independently, package the project at the command line:

	> mvn clean package

## Deploying
To install this mediator in WSO2 API Manager version 1.5, build this project and copy the resulting jar to the API Manager component library directory

	> mvn clean package
	> cp target/agave-wso2-mediator-2.0.0-SNAPSHOT.jar $CARBON_HOME/repository/components/lib
	
You will also need to copy over the org.json jar and manually deploy it into the same folder. If you build this project locally, the org.json jar can be found in your local maven repository.

	> cp ~/.m2/repository/org/json/json/20131018/json-20131018.jar $CARBON_HOME/repository/components/lib

Now that the jars are deployed, update the axis and synapse config files to use the new mediator. In the following axis files, update the following lines by appending the new filter and builder definitions.

	$CARBON_HOME/repository/conf/axis2/axis2.xml
	$CARBON_HOME/repository/conf/axis2/axis2_client.xml
	$CARBON_HOME/repository/conf/axis2/axis2_NHTTP.xml
	$CARBON_HOME/repository/conf/axis2/axis2_PT.xml
	
Replace the following line

	<messageFormatter contentType="application/json"
                        class="org.apache.axis2.json.JSONMessageFormatter"/>
                          
with

	<messageFormatter contentType="application/json"
                        class="org.apache.axis2.json.JSONMessageFormatter"/>
    <messageFormatter contentType="application/json/agave"
                        class="org.iplantc.service.wso2.mediation.AgaveJsonFormatter"/>
                          
and the following line

	<messageBuilder contentType="application/json"
                        class="org.apache.axis2.json.JSONBuilder"/>
                        
with

	<messageBuilder contentType="application/json"
                        class="org.apache.axis2.json.JSONBuilder"/>
    <messageBuilder contentType="application/json/agave"
                        class="org.iplantc.service.wso2.mediation.AgaveJsonBuilder"/>
                        
Now the API Manager and axis will convert all content passing through its ESB with content type `application/json/agave` to JSON using our custom mediator. Next we need to tell the API Manager that all errors should be given content type `application/json/agave` and run through our mediator.

Open up the synapse configs and prior to the <send/> tags at the end of the main and fault sequences, place the following line:

	<property name="messageType" value="application/json/agave" scope="axis2"/>
	
The files you need to edit are:

	$CARBON_HOME/repository/deployment/server/synapse-configs/default/sequences/main.xml
	$CARBON_HOME/repository/deployment/server/synapse-configs/default/sequences/fault.xml
	
You can also edit these through the API Manager's Sevice Bus Configuration within Carbon (Home > Manage > Service Bus > Source View > Service Bus Configuration). Do this at your own risk, however. Editing configuration documents through their GUI has been problematic for us in the past.

Once the files are updated, restart the API Manager.

	sudo service apim restart